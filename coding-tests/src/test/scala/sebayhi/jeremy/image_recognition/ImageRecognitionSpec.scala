package sebayhi.jeremy.image_recognition

import org.scalatest.flatspec.AnyFlatSpecLike
import org.scalatest.matchers.should.Matchers

class ImageRecognitionSpec extends AnyFlatSpecLike with Matchers {

  import ImageRecognition._

  it should "find a simple case" in {
    val image = Array(
      ".........",
      "......x..",
      "...xxx...",
      "........."
    )

    val pattern = Array(
      "...x",
      "xxx."
    )

    solve(image.head.length, image.length, image,
      pattern.head.length, pattern.length, pattern) should contain theSameElementsInOrderAs Array(
      1, 3
    )
  }

  it should "find that the image is the pattern" in {
    val image = Array(
      ".........",
      "......x..",
      "...xxx...",
      "........."
    )

    solve(image.head.length, image.length, image,
      image.head.length, image.length, image) should contain theSameElementsInOrderAs Array(
      0, 0
    )
  }

  it should "find a case in the top left corner" in {
    val image = Array(
      "x..x..",
      "xxx...",
      "......",
      "......"
    )

    val pattern = Array(
      "x..x",
      "xxx."
    )

    solve(image.head.length, image.length, image,
      pattern.head.length, pattern.length, pattern) should contain theSameElementsInOrderAs Array(
      0, 0
    )
  }

  it should "find a simple case in the top right corner" in {
    val image = Array(
      "......x",
      "...xxx.",
      ".......",
      "......."
    )

    val pattern = Array(
      "...x",
      "xxx."
    )

    solve(image.head.length, image.length, image,
      pattern.head.length, pattern.length, pattern) should contain theSameElementsInOrderAs Array(
      0, 3
    )
  }

  it should "find a simple case in the bottom left corner" in {
    val image = Array(
      ".........",
      ".........",
      "...x.....",
      "Bxx......"
    )

    val pattern = Array(
      "B"
    )

    solve(image.head.length, image.length, image,
      pattern.head.length, pattern.length, pattern) should contain theSameElementsInOrderAs Array(
      3, 0
    )
  }

  it should "find a simple case in the bottom right corner" in {
    val image = Array(
      ".........",
      ".........",
      "........x",
      ".....xxxA"
    )

    val pattern = Array(
      "A"
    )

    solve(image.head.length, image.length, image,
      pattern.head.length, pattern.length, pattern) should contain theSameElementsInOrderAs Array(
      3, 8
    )
  }

  it should "find a nested case" in {
    val image = Array(
      ".........",
      "..x.x....",
      "..A.B....",
      "........."
    )

    val pattern = Array(
      "x",
      "B"
    )

    solve(image.head.length, image.length, image,
      pattern.head.length, pattern.length, pattern) should contain theSameElementsInOrderAs Array(
      1, 4
    )
  }

  it should "not fall for not quite matching" in {
    val image = Array(
      ".........",
      "..x.x....",
      ".....B...",
      "........."
    )

    val pattern = Array(
      "x",
      "B"
    )

    solve(image.head.length, image.length, image,
      pattern.head.length, pattern.length, pattern) should contain theSameElementsInOrderAs Array(
      -1, -1
    )
  }

}

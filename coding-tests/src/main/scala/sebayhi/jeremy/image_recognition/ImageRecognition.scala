package sebayhi.jeremy.image_recognition

import org.slf4j.LoggerFactory

object ImageRecognition {

  private val logger = LoggerFactory.getLogger(this.getClass)

  def solve(imageWidth: Int, imageHeight: Int, image: Array[String], patternWidth: Int, patternHeight: Int, pattern: Array[String]): Array[Int] = {
    if (imageWidth < patternWidth || imageHeight < patternHeight) {
      Array(-1, -1)
    } else {
      find(imageHeight, image, patternHeight, pattern) match {
        case None => Array(-1, -1)
        case Some((x, y)) => Array(x, y)
      }
    }
  }

  private def find(
            imageHeight: Int,
            image: Array[String],
            patternHeight: Int,
            pattern: Array[String]): Option[(Int, Int)] = {
    image.foldLeft[SearchState](SearchState(0, None)) {
      case (SearchState(y, None), line) =>
        val maybeResult = lookPatternInImage(
          imageHeight = imageHeight,
          image = image,
          patternHeight = patternHeight,
          pattern = pattern,
          line = line,
          y = y
        )
        SearchState(y + 1, maybeResult)
      case (state, _) => state
    }.maybeResult
      .map(t => (t.y, t.x))
  }

  def lookPatternInImage(imageHeight: Int,
                         image: Array[String],
                         patternHeight: Int,
                         pattern: Array[String],
                         line: String,
                         y: Int): Option[SearchResult] = {
    if (line.contains(pattern.head) && y + patternHeight <= imageHeight) {
      logger.debug(s"found ${pattern.head} in $line, index was $y, ${line.indexOf(pattern.head)}")

      val searchResult = SearchResult(y = y, x = line.indexOf(pattern.head))
      if (checkFullPattern(image, pattern, searchResult)) {
        Some(searchResult)
      } else {
        logger.debug(s"nested case. line will be: ${line.drop(searchResult.x + 1)}")
        // handle nested case
        lookPatternInImage(
          imageHeight = imageHeight,
          image = image.map(_.drop(searchResult.x + 1)),
          patternHeight = patternHeight,
          pattern = pattern,
          line = line.drop(searchResult.x + 1),
          y = y
        ).map(r => r.copy(x = r.x + searchResult.x + 1))
      }
    } else {
      logger.debug(s"did not found ${pattern.head} in $line, index was $y")
      None
    }
  }

  def checkFullPattern(image: Array[String],
                       pattern: Array[String],
                       candidate: SearchResult): Boolean = {
    val reducedImage: Array[String] = {
      image.drop(candidate.y)
        .map(_.drop(candidate.x))
    }

    logger.debug("original: \n" + image.mkString("\n"))
    logger.debug("reduced image\n" + reducedImage.mkString("\n"))

    pattern.foldLeft[CheckPatternState](CheckPatternState(0, matched = true)) {
      case (CheckPatternState(index, matched), line) if matched => {
        val didMatch = reducedImage(index).startsWith(line)
        logger.debug(s"searching $line in ${reducedImage(index)}. result: $didMatch")
        CheckPatternState(
          matched = didMatch,
          index = index + 1
        )
      }
      case (state, _) => state.copy(index = state.index + 1)
    }.matched
  }
}

case class SearchResult(y: Int, x: Int)
case class SearchState(index: Int, maybeResult: Option[SearchResult])
case class CheckPatternState(index: Int, matched: Boolean)

import sbt._
import Dependencies._

object CodingTests {
  val Dependencies: Seq[sbt.librarymanagement.ModuleID] = {
      Slf4jUsingLogback.Dependencies("test") ++
      ScalaTest("test")
  }

  val Overrides: Seq[sbt.librarymanagement.ModuleID] = {
    Slf4jUsingLogback.overrides("test")
  }

  val Exclusions: Seq[ExclusionRule] = {
    Slf4jUsingLogback.ExclusionRules
  }
}

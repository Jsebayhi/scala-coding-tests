import sbt._

object Dependencies {

  def ScalaTest(scope: String) = {
    val version = "3.2.0"
    Seq(
      "org.scalatest" %% "scalatest" % version,
      "org.scalatest" %% "scalatest-flatspec" % version,
      "org.scalatest" %% "scalatest-matchers-core" % version
    ).map(_ % scope)
  }
}

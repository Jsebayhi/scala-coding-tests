import sbt._

// The goal of all this slf4j stuff is to make sure we use one sl4j logging
// implementation. We therefore override commonly used logger and replace them with
// the slf4j dedicated override.
// This will make dependencies use slf4j without knowing it.
// Doc can be found here:
// - http://www.slf4j.org/manual.html
// - http://www.slf4j.org/legacy.html#:~:text=log4j%2Dover%2Dslf4j,simply%20by%20replacing%20the%20log4j
object Slf4jUsingLogback {
  val Slf4jVersion = "1.7.30"
  val LogbackVersion = "1.2.3"

  def Dependencies(scope: String = null): Seq[sbt.librarymanagement.ModuleID] = {
    Seq(
      // Force sl4j api version to use instead of relying on dependencies' specifications
      "org.slf4j" % "slf4j-api" % Slf4jVersion
    ) ++ overrides(scope) //otherwise do not work
  }

  def overrides(scope: String = null): Seq[sbt.librarymanagement.ModuleID] = {
    val dep = Seq(
      // Override log4j 1.X
      "org.slf4j" % "log4j-over-slf4j" % Slf4jVersion,
      // Override log4j version 2.X
      "org.apache.logging.log4j" % "log4j-to-slf4j" % "2.14.1",
      // Override Jakarta common logging
      "org.slf4j" % "jcl-over-slf4j" % Slf4jVersion,
      // Override Java util logging
      "org.slf4j" % "jul-to-slf4j" % Slf4jVersion,
      // Logback
      "ch.qos.logback" % "logback-core" % LogbackVersion,
      "ch.qos.logback" % "logback-classic" % LogbackVersion
    )

    if (scope == null) {
      dep.map(_ % Runtime)
    } else {
      dep.map(_ % scope)
    }
  }

  val ExclusionRules = Seq(
    // Jakarta Common Logging and it's slf4j implementation
    ExclusionRule("commons-logging", "commons-logging"),
    ExclusionRule("org.slf4j","slf4j-jcl-1.7"),
    // java util logging
    ExclusionRule("org.slf4j", "slf4j-jdk14-1.7"),
    // Log4j 2.X and it's slf4j implementation ("org.apache.logging.log4j","log4j-slf4j-impl")
    ExclusionRule("org.apache.logging.log4j"),
    // slf4j NOOP (/dev/null) implementation
    ExclusionRule("org.slf4j", "slf4j-nop-1.7.28"),
    // slf4j simple logging
    ExclusionRule("org.slf4j", "slf4j-simple-1.7.28"),
    // Log4j 1.X and it's slf4j implementation
    ExclusionRule("log4j", "log4j"),
    ExclusionRule("org.slf4j", "slf4j-log4j12")
  )
}
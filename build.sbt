import sbt.Keys._

/**
 * Root Definition
 */

lazy val root = (project in file("."))
  .settings(
    name := "scala-coding-tests",
    Global / onChangedBuildSource := ReloadOnSourceChanges,// sbt auto reload if build.sbt changed.
    updateOptions := updateOptions.value.withLatestSnapshots(false),
    inThisBuild(Seq(
      organization := "sebayhi.jeremy",
      scalaVersion := "2.13.0",
      IntegrationTest / fork := true,
      IntegrationTest / testForkedParallel := true,
      IntegrationTest / parallelExecution  := true,
      Test / fork := true,
      Test / testForkedParallel := true,
      Test / parallelExecution := true,
      concurrentRestrictions := Seq(Tags.limitAll(2)),
      scalacOptions ++= Seq(
        "-deprecation",
        "-encoding",
        "UTF-8",
        "-feature",
        "-explaintypes",
        "-language:existentials",
        "-Xfatal-warnings",
        "-Xlint")
    ))
  ).aggregate(
  codingTests
)

/*
*** MODULES
 */

lazy val CodingTestsName = "coding-tests"
lazy val codingTests = (project in file(CodingTestsName))
  .withId(CodingTestsName)
  .settings(
    name := CodingTestsName,
    libraryDependencies ++= CodingTests.Dependencies,
    dependencyOverrides ++= CodingTests.Overrides,
    excludeDependencies ++= CodingTests.Exclusions
  )
